package hu.citec.demo.component;

import java.util.ResourceBundle;

public class EmailResourceBundle {

	private static ResourceBundle bundle = ResourceBundle.getBundle("email");

	private EmailResourceBundle() {
	}

	public static String getLocalizedString(String key) {
		return bundle.getString(key);
	}

}
