package hu.citec.demo.component;

import static hu.citec.demo.component.EmailResourceBundle.getLocalizedString;

import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import hu.citec.demo.model.dto.RegistrationEmailContent;

@Component
public class MailBuilder {

	private static final String REGISTRATION_EMAIL_TEMPLATE = "emails/registration_email";

	private TemplateEngine templateEngine;

	private RegistrationEmailContent content;
	

	public MailBuilder(TemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}

	public MailBuilder setMailContent(RegistrationEmailContent content) {
		this.content = content;
		return this;
	}

	public MailBuilder setMailContentUsername(String username) {
		check();
		this.content.setUsername(username);
		return this;
	}

	public MailBuilder setMailContentMessage(String message) {
		check();
		this.content.setMessage(message);
		return this;
	}

	public MailBuilder setMailContentGeneratedCode(String code) {
		check();
		this.content.setGeneratedCode(code);
		return this;
	}

	private void check() {
		if (this.content == null) {
			this.content = new RegistrationEmailContent();
		}
	}

	public String build() {
		return templateEngine.process(REGISTRATION_EMAIL_TEMPLATE, createContext());
	}

	private Context createContext() {
		Context context = new Context();
		addLabelsToContext(context);
		addDataToContext(context);
		return context;
	}

	private void addLabelsToContext(final Context context) {
		context.setVariable("username", getLocalizedString("username"));
		context.setVariable("code", getLocalizedString("code"));
		context.setVariable("message", getLocalizedString("message"));
	}

	private void addDataToContext(final Context context) {
		context.setVariable("content", content);
	}

}
