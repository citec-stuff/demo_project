package hu.citec.demo.exception;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

	Logger LOGGER = LoggerFactory.getLogger(ExceptionController.class);

	@ExceptionHandler(MailAlreadyUsedException.class)
	public String invalidMail(HttpServletRequest httpServletRequest, Exception ex, Model model) {
		model.addAttribute("error", "E-mail address already used: " + ex.getMessage());

		return "error/error";
	}

}
