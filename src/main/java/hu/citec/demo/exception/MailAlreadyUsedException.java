package hu.citec.demo.exception;

import org.slf4j.helpers.MessageFormatter;

public class MailAlreadyUsedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MailAlreadyUsedException(String message) {
		super(message);
	}

	public MailAlreadyUsedException(String messagePattern, Object... data) {
		super(MessageFormatter.format(messagePattern, data).getMessage());
	}

	public MailAlreadyUsedException(String message, Throwable ex) {
		super(message, ex);
	}

	public MailAlreadyUsedException(String messagePattern, Object[] data, Throwable ex) {
		super(MessageFormatter.format(messagePattern, data).getMessage(), ex);
	}
}
