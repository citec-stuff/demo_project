package hu.citec.demo.controller;

import java.util.Base64;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import hu.citec.demo.model.dto.UserRegistration;
import hu.citec.demo.service.UserService;

@Controller
@RequestMapping(path = "/form/")
public class ViewController {

	private UserService userService;

	public ViewController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping(path={"/login","/"})
	public String login() {		
		return "login";
	}

	@GetMapping("/registration")
	public String registration(Model model) {
		model.addAttribute("user", new UserRegistration());
		return "registration";
	}

	@PostMapping("/registration")
	public String saveRegistration(@ModelAttribute UserRegistration userRegistration) {
		userService.registation(userRegistration);
		// TODO: extend error msg + user notification
		return "redirect:/form/login";
	}

	@GetMapping("/main")
	public String index() throws Exception {
		return "index";
	}

	@GetMapping("/admin")
	public String admin() {
		//TODO: list users on admin page
		return "admin";
	}

	@GetMapping("/403")
	public String accessDenied() {
		return "error/403";
	}
}
