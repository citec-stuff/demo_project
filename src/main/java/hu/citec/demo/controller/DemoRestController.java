package hu.citec.demo.controller;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hu.citec.demo.model.dto.ProductFilterRequest;
import hu.citec.demo.model.entity.Product;
import hu.citec.demo.model.entity.User;
import hu.citec.demo.service.ProductService;
import hu.citec.demo.service.UserService;

@RestController
@RequestMapping(path = "/rest/", produces = MediaType.APPLICATION_JSON_VALUE)
public class DemoRestController {

	private ProductService productService;
	private UserService userService;

	public DemoRestController(ProductService productService, UserService userService) {
		this.productService = productService;
		this.userService = userService;
	}

	@GetMapping(path = "/product/all")
	@ResponseBody
	public List<Product> listAllProduct() {
		//TODO: finish the prodict list
		return Arrays.asList(new Product("TEST"));
	}

	@GetMapping(path = "/productByName", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Product>> listByName(@Validated @RequestBody ProductFilterRequest productFilterRequest) {
		return ResponseEntity.ok(productService.findByName(productFilterRequest));
	}

	@GetMapping(path = "/users/all")
	@ResponseBody
	public List<User> listAllUser() {
		return userService.findAll();
	}

}
