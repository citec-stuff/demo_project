package hu.citec.demo.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import hu.citec.demo.model.enums.Role;

@Order(1)
@Configuration
//@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// @formatter:off
        http
            .authenticationProvider(authProvider())
            //minden a form/-ra van téve --> így szépen különbséget lehet tenni a rest és a felüóleti hívás security-ben
        	.antMatcher("/form/**")
            .authorizeRequests()
            .antMatchers("/form/registration/**").permitAll()
            //felületen nem engedjük be a rest usert csak az admint és a sima usert
            .anyRequest().hasAnyRole(Role.ADMIN.name(),Role.USER.name())
        .and()
            .formLogin()
	            .loginPage("/form/login")
				.defaultSuccessUrl("/form/main", true) 
				.usernameParameter("username").passwordParameter("password") 
				.permitAll()
		.and()
			.logout()
				.invalidateHttpSession(true)
				.clearAuthentication(true) 
				.logoutRequestMatcher(new AntPathRequestMatcher("/form/logout"))
				.logoutSuccessUrl("/form/login?logout") 
				.permitAll()
		.and()
            .httpBasic().disable()
            .csrf().disable();
     // @formatter:on
	}

	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		// @formatter:off
		auth.jdbcAuthentication().dataSource(dataSource)
				.usersByUsernameQuery("SELECT username, user_password, active FROM users WHERE username = ?")
				.authoritiesByUsernameQuery(
						"SELECT u.username, r.name, u.active FROM user_role ur "
						+ "LEFT JOIN roles r ON r.id = ur.role_id "
						+ "LEFT JOIN users u ON u.id = ur.user_id "
						+ "WHERE u.username = ? AND u.active = 1");
		// @formatter:on
	}

	public DaoAuthenticationProvider authProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(NoOpPasswordEncoder.getInstance());

		return authProvider;
	}
}
