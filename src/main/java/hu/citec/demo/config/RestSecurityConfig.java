package hu.citec.demo.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import hu.citec.demo.model.enums.Role;

@Order(2)
@Configuration
public class RestSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// @formatter:off
	        http
	        	.authenticationProvider(authProvider())
	            .authorizeRequests()
	            //az összes requestnek ami /rest/-rel kezdődik admin/vagy rest joga kell legyen
	            //ezeket basic authtal beengedjük
	            .antMatchers("/rest/**").hasAnyRole(Role.REST.name(),Role.ADMIN.name())
	            .and()
	                .httpBasic()
	                .and()
	                //ne tárolja el --> stateLess legyen
	                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	            .and()
	                .csrf().disable();
	     // @formatter:on
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		// @formatter:off
		auth.jdbcAuthentication().dataSource(dataSource)
				.usersByUsernameQuery("SELECT username, user_password, active FROM users WHERE username = ?")
				.authoritiesByUsernameQuery(
						"SELECT u.username, r.name, u.active FROM user_role ur "
						+ "LEFT JOIN roles r ON r.id = ur.role_id "
						+ "LEFT JOIN users u ON u.id = ur.user_id "
						+ "WHERE u.username = ? AND u.active = 1");
		// @formatter:on
	}

	public DaoAuthenticationProvider authProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(NoOpPasswordEncoder.getInstance());

		return authProvider;
	}
}