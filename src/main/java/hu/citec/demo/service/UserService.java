package hu.citec.demo.service;

import java.util.List;

import hu.citec.demo.model.dto.UserRegistration;
import hu.citec.demo.model.entity.User;

public interface UserService {

	List<User> findAll();

	boolean mailAvailable(String mail);

	boolean registation(UserRegistration registration);
}
