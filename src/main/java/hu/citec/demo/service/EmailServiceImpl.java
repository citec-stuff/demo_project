package hu.citec.demo.service;

import java.util.Locale;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import hu.citec.demo.component.MailBuilder;
import hu.citec.demo.model.dto.RegistrationEmailContent;
import hu.citec.demo.model.dto.UserRegistration;

@Service
public class EmailServiceImpl implements EmailService {

	private JavaMailSender javaMailSender;
	private MailBuilder mailBuilder;

	public EmailServiceImpl(JavaMailSender javaMailSender, MailBuilder mailBuilder) {
		this.javaMailSender = javaMailSender;
		this.mailBuilder = mailBuilder;
	}

	@Override
	public void sendRegistrationMail(UserRegistration userRegistration) {
		String random = RandomStringUtils.random(15,true,true);
		
		mailBuilder.setMailContent(new RegistrationEmailContent(userRegistration.getUsername(),random,"TESZT"));

		sendMail(userRegistration.getEmail(), userRegistration.getDefaulLanguage());

	}
	
	 private void sendMail(String to) {
		 sendMail(to,"hu");
	 }
	
	 private void sendMail(String to,String language) {
		 System.out.println("gdfgfdg");
	        try {
				// change locale for test 
	          //  Locale.setDefault(Locale.ENGLISH);
	        	/*
	        	 * itt majd akár a user által beállított nyelvnek megfelelő mailt is ki lehet küldeni
	        	 * 
	        	 * De ha átrakjátok Locale.setDefault(Locale.ENGLISH) metódussal angolra akkor 
	        	 * angol nyelven pakolja be az emailbe a labeleket
	        	 */
	            MimeMessage mail = javaMailSender.createMimeMessage();
	            MimeMessageHelper helper = new MimeMessageHelper(mail, true);

	            helper.setTo(to);
	            helper.setSubject("REGISTRATION");
	            helper.setText(mailBuilder.build(), true);
	            javaMailSender.send(mail);
	        } catch (MessagingException e) {
	            e.printStackTrace();
	        }
	        System.out.println("OK");
	    }

}
