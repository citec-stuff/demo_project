package hu.citec.demo.service;

import hu.citec.demo.model.dto.UserRegistration;

public interface EmailService {

	void sendRegistrationMail(UserRegistration userRegistration);
}
