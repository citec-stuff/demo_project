package hu.citec.demo.service;

import java.util.List;

import hu.citec.demo.model.dto.ProductFilterRequest;
import hu.citec.demo.model.entity.Product;

public interface ProductService {

	List<Product> listAll();
	
	List<Product> findByName(ProductFilterRequest  productFilterRequest);
}
