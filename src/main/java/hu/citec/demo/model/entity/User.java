package hu.citec.demo.model.entity;

public class User {

	private Long id;
	private String name;
	private String username;
	private String email;
	private String password;
	private String registrationCode;
	private boolean active;
	private boolean loginEnabled;
	private String defaultLanguage;

	public User() {
	}

	public User(Long id, String name, String username, String email, String password, String registrationCode,
			boolean active, boolean loginEnabled, String defaultLanguage) {
		this.id = id;
		this.name = name;
		this.username = username;
		this.email = email;
		this.password = password;
		this.registrationCode = registrationCode;
		this.active = active;
		this.loginEnabled = loginEnabled;
		this.defaultLanguage = defaultLanguage;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public String getRegistrationCode() {
		return registrationCode;
	}

	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isLoginEnabled() {
		return loginEnabled;
	}

	public void setLoginEnabled(boolean loginEnabled) {
		this.loginEnabled = loginEnabled;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", username=" + username + ", active=" + active + ", loginEnabled="
				+ loginEnabled + "]";
	}

}
