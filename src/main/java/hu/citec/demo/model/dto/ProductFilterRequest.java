package hu.citec.demo.model.dto;

import javax.validation.constraints.NotNull;

public class ProductFilterRequest {

	@NotNull
	private String name;
	private boolean fullMatch;

	public ProductFilterRequest() {
	}

	public ProductFilterRequest(String name, boolean fullMatch) {
		this.fullMatch = fullMatch;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isFullMatch() {
		return fullMatch;
	}

	public void setFullMatch(boolean fullMatch) {
		this.fullMatch = fullMatch;
	}

}
