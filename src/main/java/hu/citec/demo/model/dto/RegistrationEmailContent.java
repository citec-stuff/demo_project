package hu.citec.demo.model.dto;

public class RegistrationEmailContent {

	private String username;
	private String generatedCode;
	private String message;

	public RegistrationEmailContent() {
	}

	public RegistrationEmailContent(String username, String generatedCode, String message) {

		this.username = username;
		this.generatedCode = generatedCode;
		this.message = message;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGeneratedCode() {
		return generatedCode;
	}

	public void setGeneratedCode(String generatedCode) {
		this.generatedCode = generatedCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
