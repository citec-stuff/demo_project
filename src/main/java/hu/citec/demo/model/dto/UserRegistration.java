package hu.citec.demo.model.dto;

public class UserRegistration {

	private String name;
	private String username;
	private String email;
	private String password;
	private String defaultLanguage;

	public UserRegistration() {

	}

	public UserRegistration(String name, String username, String email, String password, String defaultLanguage) {
		this.name = name;
		this.username = username;
		this.email = email;
		this.password = password;
		this.defaultLanguage = defaultLanguage;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDefaulLanguage() {
		return defaultLanguage;
	}

	public void setDefaulLanguage(String defaulLanguage) {
		this.defaultLanguage = defaulLanguage;
	}

}
